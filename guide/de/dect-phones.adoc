== icon:phone[size=fw]  DECT-Telefone

NOTE: Der Kongress stützt sich in hohem Maße auf das vom POC bereitgestellte DECT-Telefonnetz. +

Es wird nicht nur von den Teilnehmern zum Quatschen und Verabreden genutzt, sondern auch von den Kongress-Teams zur Vernetzung.

Der Himmel selbst nutzt DECT-Telefone, um Engel zu erreichen, die eine DECT-Rufnummer im Engelssystem eingetragen haben. Die Mitnahme deines DECT-Telefons zum Kongress und die Registrierung beim POC hilft dem Himmel bei vielen Aufgaben.

So könntest du beispielsweise gebeten werden, etwas zu überprüfen und kannst dein Ergebnis zurückmelden, während du noch vor Ort bist, was eine direkte Kommunikation ermöglicht. Oder du kannst während einer Türschicht schnell Informationen weitergeben, wenn jemand versucht, den Veranstaltungsort ohne gültiges Armband zu betreten.

Der POC stellt eine https://eventphone.de/doku/dect_phone_compatibility_list[Liste der kompatiblen DECT-Telefone] bereit. Wenn du zu Hause kein DECT Telefon besitzt, kannst du in der Liste nachschauen und für ca. 25 € eins im Laden kaufen.

=== Telefonbuch
* 110 Security
* 112 CERT
* 113 Awareness Team
* 1023 Heaven
* 1111 Info-Desk
