=== NOC [small]#(Network Operation Center)#

Das NOC betreibt den Cyber innerhalb der gesamten Glasfaser (und drahtlos) mit den schnellstmöglichen Geschwindigkeiten.

Sie verwalten die Internetverbindung zur Außenwelt, die Netzwerk-Switches in den Hackcentern, alle Wireless Access Points und die Colocation.
