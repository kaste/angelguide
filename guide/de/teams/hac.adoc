=== HAC [small]#(Hall Angel Coordination)#

Wenn du bereits einen Vortrag in einem Saal besucht hast und von den Engeln in den Saal und zu einem freien Platz geführt wurdest, hast du bereits die Arbeit des HAC kennengelernt.

Engel, die in einem großen Saal arbeiten und während und zwischen den Vorträgen den Fluss der Menschen in und aus dem Saal steuern, heißen Saalengel. Der Hall Angel Coordinator ist für die Koordination der Saalengel in den Sälen verantwortlich. Gemeinsam sorgen sie dafür, dass die Veranstaltung reibungslos und sicher abläuft.
