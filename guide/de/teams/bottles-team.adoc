=== Bottles Team

Das Bottles Team verwaltet den Weg der leeren Flaschen von deiner Hand zurück in den Laden, aus dem sie kamen.

Vielleicht hast du die leeren Kästen als Sammelstellen für Flaschen bemerkt.
Dieses Team entscheidet, wo diese platziert werden und bringen den gesamten Inhalt zurück ins Lager.
